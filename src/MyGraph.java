import java.util.*;

/**
 * A representation of a graph.
 * Assumes that we do not have negative cost edges in the graph.
 */
public class MyGraph implements Graph {
    // you will need some private fields to represent the graph
    // you are also likely to want some private helper methods

    // YOUR CODE HERE

//    private Vertex[] data;
//    private int[] primes = {1009,2027,4057,8117,16249,32503};// ADD MORE PRIMES
    private HashMap<Vertex,ArrayList<Edge>> data;


    /**
     * Creates a MyGraph object with the given collection of vertices
     * and the given collection of edges.
     * @param v a collection of the vertices in this graph
     * @param e a collection of the edges in this graph
     */
    public MyGraph(Collection<Vertex> v, Collection<Edge> e) {

        data = new HashMap<Vertex,ArrayList<Edge>>();

        for(Vertex v1 : v) {
            data.put(v1,new ArrayList<Edge>());
        }

        for(Edge e1: e){
            data.get(e1.getSource()).add(e1);
//            e1.next = data.get(e1.getSource());
//            data.put(e1.getSource(),e1);
        }
//	    int p = 0;
//	    while(primes[p]<v.size()){
//	        p++;
//        }
//	    data = new Vertex[primes[p]];

    }

    /** 
     * Return the collection of vertices of this graph
     * @return the vertices as a collection (which is anything iterable)
     */
    public Collection<Vertex> vertices() {

	// YOUR CODE HERE

        HashSet<Vertex> temp = new HashSet<Vertex>();

        for(Vertex v : data.keySet()){
            temp.add(new Vertex(v.getLabel()));
        }
        return temp;
    }

    /** 
     * Return the collection of edges of this graph
     * @return the edges as a collection (which is anything iterable)
     */
    public Collection<Edge> edges() {

	// YOUR CODE HERE

        HashSet<Edge> temp = new HashSet<Edge>();

        for(Vertex v : data.keySet()){
            ArrayList<Edge> t = data.get(v);
            for(Edge e : t){
                temp.add(new Edge(e.getSource(),e.getDestination(),e.getWeight()));
            }
        }

        return temp;
    }

    /**
     * Return a collection of vertices adjacent to a given vertex v.
     *   i.e., the set of all vertices w where edges v -> w exist in the graph.
     * Return an empty collection if there are no adjacent vertices.
     * @param v one of the vertices in the graph
     * @return an iterable collection of vertices adjacent to v in the graph
     * @throws IllegalArgumentException if v does not exist.
     */
    public Collection<Vertex> adjacentVertices(Vertex v)throws IllegalArgumentException{

	// YOUR CODE HERE

        if(!containsVertex(v)){
            throw new IllegalArgumentException();
        }

        HashSet<Vertex> temp = new HashSet<Vertex>();

        for(Edge e : data.get(v)){
            temp.add(new Vertex(e.getDestination().getLabel()));
        }

        return temp;
    }

    /**
     * Test whether vertex b is adjacent to vertex a (i.e. a -> b) in a directed graph.
     * Assumes that we do not have negative cost edges in the graph.
     * @param a one vertex
     * @param b another vertex
     * @return cost of edge if there is a directed edge from a to b in the graph, 
     * return -1 otherwise.
     * @throws IllegalArgumentException if a or b do not exist.
     */
    public int edgeCost(Vertex a, Vertex b)throws IllegalArgumentException{

	// YOUR CODE HERE

        if(!containsVertex(a) || !containsVertex(b)){
            throw new IllegalArgumentException();
        }
        for(Edge e : data.get(a)){
            if(e.getDestination().equals(b)){
                return e.getWeight();
            }
        }
        return -1;
    }

    private boolean containsVertex(Vertex v){
        for(Vertex v1 : data.keySet()){
            if(v.equals(v1)){
                return true;
            }
        }
        return false;
    }
}
